package demo
fun main(args : Array<String>){
    var list1: MutableList<Int> = mutableListOf(1,2,3,4,5)
    val list2: List<Int> = listOf(1,2,3,4,5,6)
    println("Immutable list is $list2")
    println("Mutable list is $list1")
    list1.add(8)    //to add an element to the mutable list
    println("Mutable list after adding 8 is $list1")
    println("1st : ${list1.first()}")   //listname.first() returns first item in the list
    println("Last : ${list1.last()}")   //listname.last() returns last item in the list
    println("2nd : ${list1[2]}")   //returns 2nd index item in the list
    var list3 = list1.subList(0, 3) //returns a sublist of list1 from index 0 to index 3
    println("Length : ${list1.size}")   //returns the size of the list
    list3.clear()
    list1.remove(1) //removes the element 1 from the list
    list1.removeAt(2)   //removes element at that particular index
    list1.forEach { n -> println("Mutable list : $n") }
}