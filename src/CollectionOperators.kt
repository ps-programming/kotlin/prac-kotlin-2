package demo

import java.util.*;
 fun main(args: Array<String>){
     val numList2 = 1..20
     val listSum = numList2.reduce{ x, y -> x + y } //this sums up all the elements of the range
     println("Reduced sum = $listSum")
     val listSum2 = numList2.fold(5){ x, y -> x + y}    //fold should have an initial value... as per the given statement the other values of the range will get added to the initial value
     println("Fold sum = $listSum2")

     println("Evens : ${numList2.any{ it % 2 == 0 }}")
     println("Evens : ${numList2.all{ it % 2 == 0 }}")

     val big3 = numList2.filter { it > 3}   //returns a list of numbers greater than 3 from the numList2
     big3.forEach{ n -> println(">3 : $n")}

     val times7 = numList2.map { it * 7 }   //multiplies every element with 7 and returns it as a list
     times7.forEach { n-> println("*7 : $n") }

 }