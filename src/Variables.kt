package demo
import java.util.Random //We can import java libraries in kotlin. Random library is used to find random values
fun main(args : Array<String>){

    println("HELLO WORLD")

    //Kotlin recognizes the data type of variables and this is called type inference
    val name = "Prabhashankar"  //recognized as string automatically. val is immutable or read only
    var myAge = 20  //recognized as integer automatically. var is mutable or changable variable

    /*Eventhough kotlin uses type inference we can give the data type manually as:
    var VariableName: Datatype = Value
    */
    var bigInt: Int = Int.MAX_VALUE
    var smallInt: Int = Int.MIN_VALUE
    println("Biggest int : " + bigInt)

    //String interpolation can be achieved using a dollar sign with a variable name within double quotes
    println("Smallest int : $smallInt")

    //Some of data types are Double, Float, Boolean, Long, Char, Short etc.

    /*When working with floating point numbers there can be loss of precision.
    With double we can have precision of 15 digits
     */
    var boo: Boolean = true //Boolean can take values true or false
    println(boo)

    var letter: Char = 'a'
    println("$letter is a character")
    println("Is $letter a character?\n${letter is Char}")
    //here @{letter is Char} checks whether letter is Char and returns true or false

    //Type Casting
    println("\n3.14 to integer : " + (3.14.toInt()))
    println("A to integer : " + ('A'.toInt()))

    //Strings
    println("\nSTRINGS")
    val myString = "hello"
    val longString = """hello
        how
        are
        you"""
    val fname = "Prabhashankar"
    val lname = "Kannapan"
    val names = fname + " " + lname //String concatenation
    println("My name is $names")
    println("1 + 3 = ${1 + 4}")
    println("String length : ${longString.length}")
    var str1 = "Hello how are you"
    var str2 = "hello how are you"
    println("Equals : ${str1.equals(str2)}")
    println("Comparing str 1 and str2 : ${str1.compareTo(str2)}")
    println("2nd Index : ${str1.get(2)}")
    println("Substring 2-4 : ${str1.substring(2,4)}")
    println("Contains random : ${str1.contains("random")}")

}
